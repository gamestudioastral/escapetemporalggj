using Inventory;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class GameManager : MonoBehaviour
{
    private int NONE_ITEM_ID = 9;

    [SerializeField]
    private ItemType m_itemOnHandType;

    [Inject(Id = "INVENTORY_CONTROLLER")]
    private InventoryController m_inventoryController;
    [Inject(Id = "ITEM_DATABASE")]
    private ItemDatabase m_itemDatabase;

    private Item m_itemOnHand;

    // Start is called before the first frame update
    void Start()
    {
            
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public Item GetItemOnHand()
    {
        if(m_itemOnHand == null)
        {
            return m_itemDatabase.GetItem(NONE_ITEM_ID);
        }

        return m_itemOnHand;
    }

    public void SetItemOnHand(ItemType itemType)
    {
        NONE_ITEM_ID = (int)itemType;
        m_itemOnHand = m_itemDatabase.GetItem(NONE_ITEM_ID);
        m_itemOnHandType = itemType;
    }

    public void ChangeItemOnHand(int oldSlot, int newSlot)
    {
        m_inventoryController.ChangeSelectedItem(oldSlot, newSlot);
    }
}
