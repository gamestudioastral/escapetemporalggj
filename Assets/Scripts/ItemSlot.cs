using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Inventory
{
    public class ItemSlot : MonoBehaviour
    {
        public ItemType itemType;
        public GameManager gameManager;

        // Start is called before the first frame update
        void Start()
        {
            var button = gameObject.GetComponent<Button>();
            var image = gameObject.GetComponent<Image>();
            button.onClick.AddListener(()=> ClickOnSlot());

            // desativa os itens inicialmente
            button.enabled = false;
            image.enabled = false;
            transform.GetChild(0).GetComponent<Image>().enabled = false;
        }

        void ClickOnSlot()
        {
            var selectedItem = gameManager.GetItemOnHand();
            
            // nenhum selecionado
            if (selectedItem.id == (int) ItemType.none)
            {
                gameManager.SetItemOnHand(itemType);
                transform.GetChild(0).GetComponent<Image>().enabled = true;
            }
            
            // se clicar em outro
            //BUGADO
            else if (selectedItem.id != (int) itemType)
            {
                gameManager.ChangeItemOnHand(oldSlot: selectedItem.id, newSlot: (int) itemType) ;
            }

            // se clicar no mesmo
            else
            {
                gameManager.SetItemOnHand(ItemType.none);
                var child = transform.GetChild(0);
                child.gameObject.GetComponent<Image>().enabled = false;
            }
        }

        void UseItem()
        {
            //gameObject.GetComponent<Button>().enabled = false;
            gameObject.GetComponent<Image>().enabled = false;
            transform.GetChild(0).GetComponent<Image>().enabled = false;
        }
    }
}