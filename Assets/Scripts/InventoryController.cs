using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Inventory 
{ 
    public class InventoryController : MonoBehaviour
    {
        public List<Item> playerItems = new List<Item>();
        public ItemDatabase itemDatabase;
        private List<GameObject> itemStots = new List<GameObject>();

        [SerializeField]
        private Animator m_animator;

        // Start is called before the first frame update
        void Start()
        {
            //GiveItem(0);
            foreach (Transform child in transform)
            {
                itemStots.Add(child.gameObject);
            }
        }

        public void GiveItem(int id)
        {
            Item itemToAdd = itemDatabase.GetItem(id);
            playerItems.Add(itemToAdd);

            UpdateBar();

            Debug.Log("Item adicionado: " + itemToAdd.title);
        }

        public Item CheckForItem(int id)
        {
            return playerItems.Find(item => item.id == id);
        }

        public Item RemoveItem(int id)
        {
            Item itemToRemove = CheckForItem(id);

            if (itemToRemove != null)
            {
                playerItems.Remove(itemToRemove);
            }

            Debug.Log("Item removido: " + itemToRemove.title);
            return itemToRemove;
        }

        // adiciona e remove itens na barra
        // TODO: implementar remo��o de itens na barra
        public void UpdateBar()
        {
            foreach (Item item in playerItems)
            {
                itemStots[item.id].GetComponent<Button>().enabled = true;
                itemStots[item.id].GetComponent<Image>().enabled = true;
            }
        }

        // desabilita o contorno do item antigo e habilita o contorno do novo
        public void ChangeSelectedItem(int oldSlot, int newSlot)
        {
            var oldBackground = itemStots[oldSlot].transform.GetChild(0);
            oldBackground.GetComponent<Image>().enabled = false;

            var newBackground = itemStots[newSlot].transform.GetChild(0);
            newBackground.GetComponent<Image>().enabled = true;
        }

        public void SwitchInventory()
        {
            m_animator.SetTrigger("Switch");
        }
    }
}